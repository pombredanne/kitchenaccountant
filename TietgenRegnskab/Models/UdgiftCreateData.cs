﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TietgenRegnskab.Models
{
    public class UdgiftCreateData
    {
        //The two properties
        public Udgift Udgift { get; set; }
        public Dictionary<Beboer,bool> BeboerChecklist { get; private set; }

        //A parameterless constructor
        public UdgiftCreateData()
        {
            this.Udgift = new Udgift();
            this.BeboerChecklist = new Dictionary<Beboer,bool>();
        }

        //A construction to instanciate
        public UdgiftCreateData(Udgift udgift, IEnumerable beboere)
        {
            bool found;
            Udgift = udgift;
            BeboerChecklist = new Dictionary<Beboer, bool>();

            foreach (Beboer beboer in beboere)
            {
                found = false;
                foreach (var udgiftsdeltager in udgift.Beboers)
                {
                    if (beboer.ID == udgiftsdeltager.ID)
                    {
                        BeboerChecklist.Add(beboer, true);
                        found = true;
                        break;
                    }
                }
                if (!found)
                    BeboerChecklist.Add(beboer, false);

            }
        }

    }
}