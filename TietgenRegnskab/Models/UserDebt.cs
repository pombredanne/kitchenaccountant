﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TietgenRegnskab.Models
{
    public class UserDebt
    {
        public string Name { get; set; }
        public int Debt { get; set; }
    }
}