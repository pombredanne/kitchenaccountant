//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TietgenRegnskab.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Udgift
    {
        public Udgift()
        {
            this.Beboers = new HashSet<Beboer>();
        }
    
        public int ID { get; set; }
        public int Beboer_ID { get; set; }
        public decimal Beloeb { get; set; }
        public string Beskrivelse { get; set; }
        public System.DateTime Dato { get; set; }
    
        public virtual Beboer Beboer { get; set; }
        public virtual ICollection<Beboer> Beboers { get; set; }
    }
}
