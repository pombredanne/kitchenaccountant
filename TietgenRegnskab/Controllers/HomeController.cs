﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TietgenRegnskab.Models;

namespace TietgenRegnskab.Controllers
{
    public class HomeController : Controller
    {
        
        public ViewResult Index()
        {   
            ViewBag.Title = "18.5 Kitchen Accounting";
            ViewBag.Message = "Show me the money!";

            return View();
        }

        public ActionResult About()
        {
            ViewBag.location = "Copenhagen";
            return View();
        }
    }
}
