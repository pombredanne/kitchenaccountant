﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TietgenRegnskab.Models;

namespace TietgenRegnskab.Controllers
{
    public class DrukController : Controller
    {
        TE _db = new TE();
        //
        // GET: /Druk/

        public ActionResult Index()
        {
            var model = _db.Druk_Rapport;
            return View(model);
        }

        //
        // GET: /Druk/Details/5
        [Authorize]
        public ActionResult Details(int id)
        {            
            Druk_Rapport druk = _db.Druk_Rapport.Single(D => D.ID == id);
            return View(druk);
        }

        //
        // GET: /Druk/Create
        [Authorize]
        public ActionResult Create()
        {          
            ViewBag.Druk = new Druk();
            ViewBag.Beboere = _db.Beboers;
            return View();
        } 

        //
        // POST: /Druk/Create
        [Authorize]
        [HttpPost]
        public ActionResult Create(Druk druk)
        {
            try
            {
                _db.Druks.Add(druk);
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        
        //
        // GET: /Druk/Edit/5
        [Authorize] 
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Druk/Edit/5
        [Authorize]
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Druk/Delete/5
 [Authorize]
        public ActionResult Delete(int id)
        {
            Druk_Rapport druk = _db.Druk_Rapport.Single(D => D.ID == id);
            return View(druk);
        }

        //
        // POST: /Druk/Delete/5
        [Authorize]
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                Druk druk = _db.Druks.Single(D => D.ID == id);
                _db.Druks.Remove(druk);
                _db.SaveChanges();
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
