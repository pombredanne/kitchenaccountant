﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TietgenRegnskab.Models;

namespace TietgenRegnskab.Controllers
{
    public class TotalController : Controller
    {
        TE _db = new TE();
        //
        // GET: /Total/

        public ActionResult Index()
        {
            var model = _db.Total_Rapport;
            return View(model);
        }

        //
        // GET: /Total/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Total/Create

        public ActionResult Create()
        {
            return View();
        } 

        //
        // POST: /Total/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        
        //
        // GET: /Total/Edit/5
 
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Total/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Total/Delete/5
 
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Total/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
