﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TietgenRegnskab.Models;


namespace TietgenRegnskab.Controllers
{
    public class UdgiftController : Controller
    {
        TE _db = new TE();
        //
        // GET: /Udgift/


        public ActionResult Index(string beboerString,string takerString)
        {
            var model = from u in _db.Udgifts
                        select u;
            if(!String.IsNullOrEmpty(beboerString))
            {
                model = model.Where(u => u.Beboer.Name.Contains(beboerString));
            }
            if (!String.IsNullOrEmpty(takerString))
            {
                model = model.Where(u => u.Beboers.Contains(_db.Beboers.FirstOrDefault(b => b.Name.Contains(takerString))));
            }
            return View(model);
        }

        //
        // GET: /Udgift/Details/5

        public ActionResult Details(int id)
        {
            var udgift = _db.Udgifts.SingleOrDefault(U=>U.ID==id);
            var beboers = _db.Beboers;
            var model = new UdgiftCreateData(udgift, beboers);
            return View(model);
        }

        //
        // GET: /Udgift/Create

        public ActionResult Create()
        {
            return View(new UdgiftCreateData(new Udgift(),_db.Beboers.Where(B=>B.Moved_out==null)));
        } 

        //
        // POST: /Udgift/Create

        [HttpPost]
        public ActionResult Create(UdgiftCreateData udgiftdata,FormCollection postedForm)
        {
            try
            {
                List<Beboer> beboereToAdd = new List<Beboer>();
                Udgift udgift = udgiftdata.Udgift;
                var beboere = _db.Beboers.Where(B => B.Moved_out == null);
                foreach (var beboer in beboere)
                {
                    if (postedForm[beboer.Name].ToString().Contains("true"))
                    {
                        udgift.Beboers.Add(beboer);
                    }
                    else if(udgift.Beboers.Contains(beboer))
                        udgift.Beboers.Remove(beboer);
                }

                _db.Udgifts.Add(udgift);
                _db.SaveChanges();

                return RedirectToAction("Index");

            }
            catch
            {
                return View();
            }
        }

        
        //
        // GET: /Udgift/Edit/5


        public ActionResult Edit(int id)
        {
            var udgift = _db.Udgifts.SingleOrDefault(U => U.ID == id);
            var model = new UdgiftCreateData(udgift, _db.Beboers.Where(B => B.Moved_out == null));
            return View(model);
        }

        //
        // POST: /Udgift/Edit/5

        [HttpPost]
        public ActionResult Edit(int id,UdgiftCreateData udgiftdata, FormCollection postedForm)
        {
            try
            {
                List<Beboer> beboereToAdd = new List<Beboer>();
                Udgift udgift = udgiftdata.Udgift;
                var beboere = _db.Beboers.Where(B => B.Moved_out == null);
                foreach (var beboer in beboere)
                {
                    if (postedForm[beboer.Name].ToString().Contains("true"))
                    {
                        udgift.Beboers.Add(beboer);
                    }
                    else if (udgift.Beboers.Contains(beboer))
                        udgift.Beboers.Remove(beboer);
                }

                _db.Udgifts.Remove(_db.Udgifts.First(u => u.ID == id));
                _db.Udgifts.Add(udgift);
                _db.SaveChanges();

                return RedirectToAction("Index");

            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Udgift/Delete/5
        [Authorize]
        public ActionResult Delete(int id)
        {
            Udgift udgift = _db.Udgifts.Single(U => U.ID == id);
            return View(udgift);
        }

        //
        // POST: /Udgift/Delete/5
        [Authorize]
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                Udgift udgift = _db.Udgifts.Single(U => U.ID == id);
                _db.Udgifts.Remove(udgift);
                _db.SaveChanges();
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
