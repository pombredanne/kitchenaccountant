﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TietgenRegnskab.Models;

namespace TietgenRegnskab.Controllers
{
    public class PaymentController : Controller
    {
        TE _db = new TE();
        //
        // GET: /Payment/

        public ActionResult Index()
        {
            var mode = _db.Indbetalings;
            return View(mode);
        }

        //
        // GET: /Payment/Details/5
        [Authorize]
        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Payment/Create
        [Authorize]
        public ActionResult Create()
        {
            return View(new Indbetaling(_db.Beboers));
        } 

        //
        // POST: /Payment/Create
        [Authorize]
        [HttpPost]
        public ActionResult Create(Indbetaling indbetaling,FormCollection collection)
        {
            try
            {
                _db.Indbetalings.Add(indbetaling);
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        
        //
        // GET: /Payment/Edit/5
 [Authorize]
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Payment/Edit/5
        [Authorize]
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Payment/Delete/5
        [Authorize]
        public ActionResult Delete(int id)
        {
            Indbetaling var = _db.Indbetalings.Single(i => i.ID == id);
            return View(var);
        }

        //
        // POST: /Payment/Delete/5
        [Authorize]
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                Indbetaling indbetaling = _db.Indbetalings.Single(I => I.ID == id);
                _db.Indbetalings.Remove(indbetaling);
                _db.SaveChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
