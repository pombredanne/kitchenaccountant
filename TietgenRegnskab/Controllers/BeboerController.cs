﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TietgenRegnskab.Models;

namespace TietgenRegnskab.Controllers
{
    public class BeboerController : Controller
    {

        TE _db = new TE();
        //
        // GET: /Beboer/

        public ActionResult Index()
        {
            var model = _db.Beboers; 
            return View(model);
        }

        //
        // GET: /Beboer/Details/5
        [Authorize]
        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Beboer/Create
        [Authorize]
        public ActionResult Create()
        {
            return View(new Beboer());
        } 

        //
        // POST: /Beboer/Create
        [Authorize]
        [HttpPost]
        public ActionResult Create(Beboer beboerIn,FormCollection collection)
        {
            try
            {
                Beboer beboer = beboerIn;
                _db.Beboers.Add(beboer);
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        
        //
        // GET: /Beboer/Edit/5
 [Authorize]
        public ActionResult Edit(int id)
        {
            var beboer = _db.Beboers.Single(B => B.ID == id);
            return View(beboer);
        }

        //
        // POST: /Beboer/Edit/5
        [Authorize]
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here
                _db.Beboers.Single(B => B.ID == id).Name = collection[4].ToString();
                _db.SaveChanges();
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Beboer/Delete/5
 [Authorize]
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Beboer/Delete/5
        [Authorize]
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
