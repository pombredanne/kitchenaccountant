﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TietgenRegnskab.Controllers;

namespace TietgenRegnskab.Test
{
    [TestFixture]
    public class HomeControllerIndexTest
    {
        [Test]
        public void Puts_message_in_viewBag()
        {
            var controller = new HomeController();
            var result = controller.Index();
            Assert.IsNotNull(result.ViewBag.Message);
        }
    }
}
